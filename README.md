# SVM Recognise Handwriting

This project attempts to recognise handwriting. The project is in jupyter notebook and consists of preprocessing of handwritten letters and uses Support Vector machines in an attempt at recognising the letters.

## Install
To install the script requires that you have jupyter notebook installed. Install (Anaconda)[www.anaconda.com] and select jupyter notebook.

## Usage
To run the script open the file handWritingRecognise.ipynb. The script handles dependencies when you run it.

## Contributing
Send questions and suggestions to <thomas.haaland@gmail.com>

UNLICENSED
